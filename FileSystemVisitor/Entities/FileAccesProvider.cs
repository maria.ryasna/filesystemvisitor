﻿using FileSystemVisitor.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSystemVisitor.Entities
{
    public class FileAccesProvider: IFileAccessProvider
    {
        public IEnumerable<string> GetFiles(string path)
        {
            return Directory.EnumerateFiles(path);
        }

        public IEnumerable<string> GetDirectories(string path)
        {
            return Directory.EnumerateDirectories(path);
        }

        public bool IsPathExists(string path)
        {
            return Directory.Exists(path);
        }
    }
}
