﻿using FileSystemVisitor.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileSystemVisitor.Entities
{
    public class FileSystemItem
    {
        public ItemType Type { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
