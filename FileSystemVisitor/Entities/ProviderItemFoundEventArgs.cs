﻿using FileSystemVisitor.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileSystemVisitor.Entities
{
    public class ProviderItemFoundEventArgs: EventArgs
    {
        public FileSystemItem Item { get; set; }
        public bool StopSearch { get; set; }
        public bool ExcludeItem { get; set; }
    }
}
