﻿using FileSystemVisitor.Entities;
using FileSystemVisitor.Enums;
using FileSystemVisitor.Interfaces;
using FileSystemVisitor.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSystemVisitor
{
    public class FileSystemVisitor : IEnumerable<FileSystemItem>
    {
        private string path { get; set; }
        private IFileAccessProvider provider { get; set; }
        private Predicate<FileSystemItem> predicate { get; set; }

        public event EventHandler SearchStarted;
        public event EventHandler SearchFinished;
        public event EventHandler<ProviderItemFoundEventArgs> FileFound;
        public event EventHandler<ProviderItemFoundEventArgs> DirectoryFound;
        public event EventHandler<ProviderItemFoundEventArgs> FilteredFileFound;
        public event EventHandler<ProviderItemFoundEventArgs> FilteredDirectoryFound;
        
        public FileSystemVisitor(string path)
        {
            this.path = path;
            provider = new FileAccesProvider();
            predicate = null;
        }

        public FileSystemVisitor(string path, Predicate<FileSystemItem> predicate)
        {
            this.path = path;
            provider = new FileAccesProvider();
            this.predicate = predicate;
        }

        public IEnumerable<FileSystemItem> GetFileSystemTree(string path)
        {
            if (path is null)
            {
                throw new FileAccessProviderException($"Argument {nameof(path)} is null. Path can not be null.");
            }

            if (!provider.IsPathExists(path))
            {
                throw new FileAccessProviderException($"Argument {nameof(path)} is invalid or does not exist.");
            }

            OnSearchStarted(); 

            var items = Enumerate(path);
            foreach (var item in items)
            {
                yield return item;
            }

            OnSearchFinished();
        }

        private IEnumerable<FileSystemItem> Enumerate(string path)
        {
            var files = provider.GetFiles(path);

            foreach (var file in files)
            {
                var fileItem = CreateFileSystemItem(file, ItemType.File);
                var providerEventArgs = GetProviderEventArgs(fileItem);
                OnFileFound(providerEventArgs);

                if (providerEventArgs.StopSearch)
                {
                    yield break;
                }

                if (!providerEventArgs.ExcludeItem)
                {
                    if (!(predicate is null) && predicate(fileItem))
                    {
                        OnFilteredFileFound(providerEventArgs);
                        yield return fileItem;
                    }
                    else if (predicate is null)
                    {
                        yield return fileItem;
                    }
                }
            }

            var directories = provider.GetDirectories(path);

            foreach (var directory in directories)
            {
                var directoryItem = CreateFileSystemItem(directory, ItemType.Directory);
                var providerEventArgs = GetProviderEventArgs(directoryItem);
                OnDirectoryFound(providerEventArgs);

                if (providerEventArgs.StopSearch)
                {
                    yield break;
                }

                if (!providerEventArgs.ExcludeItem)
                {
                    if (!(predicate is null) && predicate(directoryItem))
                    {
                        OnFilteredDirectoryFound(providerEventArgs);
                        yield return directoryItem;
                    } 
                    else if (predicate is null)
                    {
                        yield return directoryItem;
                    }
                }

                foreach (var item in Enumerate(directory))
                {
                    yield return item;
                }
            }         
        }

        private ProviderItemFoundEventArgs GetProviderEventArgs(FileSystemItem item) {
            return new ProviderItemFoundEventArgs
            {
                Item = item,
                StopSearch = GetStopSearchFlag(item),
                ExcludeItem = GetExcludeItemFlag(item)
            };
        }

        private FileSystemItem CreateFileSystemItem(string path, ItemType type)
        {
            string name = type == ItemType.Directory ? new DirectoryInfo(path).Name : new FileInfo(path).Name;
            return new FileSystemItem
            {
                Path = path,
                Name = name,
                Type = type
            };
        }

        private bool GetStopSearchFlag(FileSystemItem item)
        {
            if (item.Type == ItemType.File)
            {
                return item.Name.Contains(".DAT") || item.Name.Contains(".cs");
            }
            else
            {
                return item.Name.Contains("system") || item.Name.Contains("setup");
            }
        }

        private bool GetExcludeItemFlag(FileSystemItem item)
        {
            if (item.Type == ItemType.File)
            {
                return item.Name.Contains(".exe") || item.Name.Contains(".json");
            }
            else
            {
                return item.Name.Contains("Microsoft") || item.Name.Contains("Windows");
            }
        }

        public IEnumerator<FileSystemItem> GetEnumerator()
        {
            return GetFileSystemTree(path).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void OnSearchStarted()
        {
            SearchStarted.Invoke(this, EventArgs.Empty);
        }

        private void OnSearchFinished()
        {
            SearchFinished.Invoke(this, EventArgs.Empty);
        }

        private void OnFileFound(ProviderItemFoundEventArgs eventArgs)
        {
            FileFound.Invoke(this, eventArgs);
        }

        private void OnDirectoryFound(ProviderItemFoundEventArgs eventArgs)
        {
            DirectoryFound.Invoke(this, eventArgs);
        }

        private void OnFilteredFileFound(ProviderItemFoundEventArgs eventArgs)
        {
            FilteredFileFound.Invoke(this, eventArgs);
        }

        private void OnFilteredDirectoryFound(ProviderItemFoundEventArgs eventArgs)
        {
            FilteredDirectoryFound.Invoke(this, eventArgs);
        }
    }
}
