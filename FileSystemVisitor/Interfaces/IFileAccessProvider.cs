﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileSystemVisitor.Interfaces
{
    public interface IFileAccessProvider
    {
        IEnumerable<string> GetFiles(string path);
        IEnumerable<string> GetDirectories(string path);
        bool IsPathExists(string path);
    }
}
