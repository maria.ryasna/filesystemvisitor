﻿using FileSystemVisitor.Entities;
using FileSystemVisitor.Enums;
using FileSystemVisitor.Validation;
using System;
using System.Collections.Generic;
using System.IO;

namespace FileSystemVisitor
{
    public class Program
    {
        static void Main(string[] args)
        {
            const string path = @"C:\Test";
            Predicate<FileSystemItem> predicate = (item) => item.Type == ItemType.Directory;
            FileSystemVisitor fileSystemVisitor = new FileSystemVisitor(path, predicate);
            var listOfItems = new List<FileSystemItem>();

            fileSystemVisitor.SearchStarted += (sender, eventArgs) => Console.WriteLine("[INFO]: Searching started.");
            fileSystemVisitor.SearchFinished += (sender, eventArgs) => Console.WriteLine("[INFO]: Searching finished.");
            fileSystemVisitor.FileFound += (sender, eventArgs) => Console.WriteLine($"[SUCCESS]: { eventArgs.Item.Type } { eventArgs.Item.Name } founded.");
            fileSystemVisitor.DirectoryFound += (sender, eventArgs) => Console.WriteLine($"[SUCCESS]: { eventArgs.Item.Type } { eventArgs.Item.Name } founded.");
            fileSystemVisitor.FilteredDirectoryFound += (sender, eventArgs) => Console.WriteLine($"[INFO]: Filtered { eventArgs.Item.Type } : { eventArgs.Item.Name }.");
            fileSystemVisitor.FilteredFileFound += (sender, eventArgs) => Console.WriteLine($"[INFO]: Filtered { eventArgs.Item.Type } : { eventArgs.Item.Name }.");
            
            try 
            {
                foreach (var item in fileSystemVisitor)
                {
                    listOfItems.Add(item);
                }
            }
            catch (FileAccessProviderException exception)
            {
                Console.WriteLine($"[ERROR]: {exception.Message}.");
            }
            catch (Exception)
            {
                Console.WriteLine($"[ERROR]: Unhandled exception.");
            }
            ShowListOfItems(listOfItems);
        }

        public static void ShowListOfItems(List<FileSystemItem> list)
        {
            if (list.Count == 0)
            {
                Console.WriteLine("There are no file or directory in result list.");
            } 
            else
            {
                Console.WriteLine("Founded files and directories:");
                foreach (var item in list)
                {
                    Console.WriteLine("{0} name: {1}", item.Type, item.Name);
                    Console.WriteLine("Path: {0}", item.Path);
                    Console.WriteLine();
                }
            }
        }
    }
}
